const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const Chart = require('chart.js');
const ejs = require('ejs');
const request = require('request');


app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
  extended: true
}));



app.set('view engine', 'ejs');

app.get("/", function(req, res) {

  let data_left = [];
  let data_right = [];
  let text = "waiting";
  let colour = "blue";
  let display = false;

  res.render('list', {
    data_left: data_left,
    data_right: data_right,
    text: text,
    colour: colour,
    display: false,
  });
});

app.post("/", function(req, res) {

  let data_left = Object.values([
    req.body.L500,
    req.body.L1k,
    req.body.L2k,
    req.body.L3k,
    req.body.L4k,
    req.body.L6k,
    req.body.L8k,
  ])

  let data_right = Object.values([
    req.body.R500,
    req.body.R1k,
    req.body.R2k,
    req.body.R3k,
    req.body.R4k,
    req.body.R6k,
    req.body.R8k,
  ])

  let apiData = data_left.concat(data_right);

  let raw_data = {
    "data": apiData
  };

  let data = JSON.stringify(raw_data)

  let options = {
    url: "http://a7091c3f-779c-4930-8cce-820c012aee6a.westeurope.azurecontainer.io/score",
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: data,
  };

  request(options, function(error, response, body) {
    let x = JSON.parse(body);
    var value = x.score_samples;
    console.log(value);

    if (value > -60) {

      let text = "Verified";
      let colour = "green";
      let display = true

      res.render('list', {
        data_left: data_left,
        data_right: data_right,
        text: text,
        colour: colour,
        display: display
      });
    } else {

      let text = "Unverified";
      let colour = "red";
      let display = true

      res.render('list', {
        data_left: data_left,
        data_right: data_right,
        text: text,
        colour: colour,
        display: display
      });
    }
  });

});
app.listen(process.env.PORT || 3000, function(req, res) {
  console.log("Server started on port 3000");

});
